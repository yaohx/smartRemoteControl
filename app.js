//引入mqtt库
import mqtt from 'utils/mqtt.js'; 
App({
  onLaunch: function () {
    //链接mqtt服务器
    this.connectMqttServer();
  },
  globalData: {
    userOpenId:null,
    connectStatus: false,
    driverCode:"",
    driverList:[],
    serverHost:"https://smarthome.yaohx.cn/"
  }, 
  data:{
    mqtt: {
      client:null,
      host: "wxs://mqtt.yaohx.cn/mqtt",
      //记录重连的次数
      reconnectCounts: 0,
      //MQTT连接的配置
      options: {
        protocolVersion: 4, //MQTT连接协议版本
        clientId: 'mqttWx_' + new Date().getTime(),
        clean: false,
        reconnectPeriod: 1000, //1000毫秒，两次重新连接之间的间隔
        connectTimeout: 30 * 1000, //1000毫秒，两次重新连接之间的间隔
        resubscribe: true //如果连接断开并重新连接，则会再次自动订阅已订阅的主题（默认true）
      }
    }
  },


  getServices: function (userOpenId, callback) {
    var app = this;
    if (this.globalData.driverList.length===0){
      console.log("后台查询设备列表")
      var url = this.globalData.serverHost + userOpenId + "/getServices";
      wx.request({
        url: url,
        success: function (res) {
          app.globalData.driverList = res.data;
          callback(res.data);
        },
        fail: function () {
          wx.hideLoading();
          wx.showToast({
            title: '加载失败',
            icon: 'none',
            duration: 2000
          })
        }
      })
    }else{
      callback(this.globalData.driverList);
    }
  },

  /**链接 mqtt服务器 */
  connectMqttServer: function () {
    var that = this;
    //开始连接
    this.data.client = mqtt.connect(this.data.mqtt.host,this.data.mqtt.options);

    this.data.client.on('connect', function (connack) {
      that.setConnectStatus(true);
      console.log("connect success"); 
    })


    //服务器下发消息的回调
    this.data.client.on("message", function (topic, payload) {
      var currentPage = getCurrentPages()[0];
      if (currentPage.showMessage){
        currentPage.showMessage(topic, payload);
      }
    })

    //服务器连接异常的回调
    this.data.client.on("error", function (error) {
      that.setConnectStatus(false);
      console.log(" 服务器 error 的回调" + error)

    })

    //服务器重连连接异常的回调
    this.data.client.on("reconnect", function () {
      that.setConnectStatus(false);
      console.log(" 服务器 reconnect的回调")
    })


    //服务器连接异常的回调
    this.data.client.on("offline", function (errr) {
      that.setConnectStatus(false);
      console.log(" 服务器offline的回调")
    })


    //服务器连接异常的回调
    this.data.client.on("close", function (errr) {
      that.setConnectStatus(false);
      console.log(" 服务器close的回调")
    })

  },
  setConnectStatus: function (flag) {
    this.globalData.connectStatus = flag;
    if (flag) {
      wx.setNavigationBarTitle({
        title: '远程LED灯控制(已连接)'
      });
      wx.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: '#76EE00',
      })
    } else {
      wx.setNavigationBarTitle({
        title: '远程LED灯控制(未连接)'
      });
      wx.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: '#ff0000',
      })
    }
  },
  subTopic: function (outTopic) {
    console.log(" 订阅 " + outTopic)
    if (this.data.client && this.data.client.connected) {
      //仅订阅单个主题
      this.data.client.subscribe(outTopic, function (err, granted) {
        if (!err) {
          console.log(" 订阅 "+outTopic+"  成功") 
          wx.showToast({
            title: " 订阅 " + outTopic + "  成功",
            icon: 'none',
            duration: 2000
          })
        } else {
          console.log(" 订阅 "+outTopic+" 失败") 
          wx.showToast({
            title: " 订阅 " + outTopic + " 失败",
            icon: 'none',
            duration: 2000
          })
        }
      })
    } else {
      console.log("not connect");
      wx.showToast({
        title: "请先链接服务器",
        icon: 'none',
        duration: 2000
      })
    }
  },
  pubMsg: function (msg) {
    var inTopic = this.globalData.driverCode + "_InTopic"
    console.log(" 发布信息[" + inTopic+"]   " + msg)
    if (this.data.client && this.data.client.connected) {
      this.data.client.publish(inTopic, msg);
    } else {
      wx.showToast({
        title: '请先连接服务器',
        icon: 'none',
        duration: 2000
      })
    }
  },
  getUserOpenId:function(callback){
    var app = this;
    if (app.globalData.userOpenId === null){
      console.log("获取 userOpenId");
      //登录，获取用户唯一标识
      wx.login({
        success: function (res) {
          console.log("登录成功："+res.code)
          var code = res.code; //返回code
          //请求用户信息
          wx.request({
            url: 'https://smarthome.yaohx.cn/getUserOpenId',
            data: {
              code: code,
            },
            header: {
              'content-type': 'json'
            },
            success: function (res) {
              console.log(res);
              var openid = res.data.openid //返回openid
              console.log(openid)
              app.globalData.userOpenId = openid;
              callback(openid);
            }
          })
        },fail: function (res) {
          console.log(res);
          console.log('is failed')
        }
        
      })
    }else{
      callback(app.globalData.userOpenId);
    }
  }
})