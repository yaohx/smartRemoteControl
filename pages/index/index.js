const app = getApp()
Page({
  data: {
    servers: [],
    driverCode:"test"
  },

  onShow: function () {
    wx.showLoading({
      title: '加载中。。。',
      mask: true
    })
    var page = this;
    app.getUserOpenId(function (userOpenId) {
      console.log(userOpenId);
      var listService = app.getServices(userOpenId, function (listService) {
        console.log(listService);
        page.setData({
          servers: listService
        });
        wx.hideLoading();
      });
    });
  },
  onLoad: function () {

  },


addDriver:function(){
  var that = this;
  wx.scanCode({
    success:function(res){
      var jsonStr = res.result;
      //去掉多余的字符，防止转json时报错
      jsonStr = jsonStr.replace(/\ufeff/g, "");
      var scanDriver = JSON.parse(jsonStr);
      console.log(scanDriver);
      scanDriver.userOpenId = app.globalData.userOpenId;
      scanDriver.enabled = true;
      var url = "https://smarthome.yaohx.cn/registDriver";
      wx.request({
        url: url,
        header: {
          'Content-Type': 'application/json;charset=UTF-8'
        },
        data: scanDriver,
        success: function (res) {
          var listService = that.data.servers;
          listService.push(res.data);
          that.setData({
            servers: listService
          });
        },
        fail: function () {
         
        }
      })
    }
  });
}, 
  /**
   * 当点击Item的时候传递过来
   */
  bindNavigator: function (e) {
    var code = e.currentTarget.dataset.code;
    app.globalData.driverCode = code;
    wx.redirectTo({
      url: e.currentTarget.dataset.path
    })
  }
})