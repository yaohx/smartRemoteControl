var app = getApp();
Page({
  data: {
    r: 255,
    g: 255,
    b: 255,
    max:255,
    disableColor: "",
    SingleColor:true,
    colour:false,
    Dazzle:false
  }, 
  /**
   * 颜色条调整时触发事件
   */
  changeing(e) {
    let color = e.currentTarget.dataset.color
    let value = e.detail.value;
    if (color == "all") {
      this.setData({
        r: value,
        g: value,
        b: value,
        max:value
      });
    } else {
      this.setData({
        [color]: value
      });
      var max = this.data.r;
      if (this.data.g>max){
        max = this.data.g;
      }
      if (this.data.b>max){
        max = this.data.b;
      }
      this.setData({
        max: max
      });
    }
    var colorData = { r: this.data.r, g: this.data.g, b: this.data.b }; 
    app.pubMsg(JSON.stringify(colorData)); 
  },
  /**
   * 模式切换时触发事件
   */
  radioChange: function (e) {
    var m = e.detail.value;
    if("s" === m){
      console.log(m);
      var colorData = { r: this.data.r, g: this.data.g, b: this.data.b };
      this.setData({ disableColor:false});
      app.pubMsg(JSON.stringify(colorData)); 
    }else {
      console.log(m);
      this.setData({ disableColor: "disabled" });
      app.pubMsg(m); 
    }
  },
  showMessage(topic, payload){
    if (this.data.driverCode + "_OutTopic"){
      var msg = String(payload);
      var json = JSON.parse(msg);
      var mode = json.mode;
      var r = json.r;
      var g = json.g;
      var b = json.b;
      if ("SingleColor" == mode){
        var max = json.r;
        if (json.g > max) {
          max = json.g;
        }
        if (json.b > max) {
          max = json.b;
        }
        json.max = max;
        json.disableColor = false;
        console.log(json)
        var setDateInfo = {
          r: r,
          g: g,
          b: b,
          max: max,
          disableColor: "",
          SingleColor: true,
          colour: false,
          Dazzle: false
        };
        this.setData(setDateInfo);
      } else if ("colour" == mode) {
        this.setData({
          disableColor: "disabled",
          SingleColor: false,
          colour: true,
          Dazzle: false
        });
      } else if ("Dazzle" == mode) {
        this.setData({
          disableColor: "disabled",
          SingleColor: false,
          colour: false,
          Dazzle: true
        });
      }
    }
  },
  onLoad: function () {
    this.setData({ "driverCode": app.globalData.driverCode });
    app.setConnectStatus(app.globalData.connectStatus);
    var outTopic = this.data.driverCode + "_OutTopic"
    app.subTopic(outTopic);
  }
})