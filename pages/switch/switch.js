var app = getApp();
Page({
  data: {
    switchChecked: false
  }, 
  onSwitchChange(e) {
    let value = e.detail.value;
    if (value) {
      app.pubMsg("0");
    } else {
      app.pubMsg("1");
    }
  }, 
  showMessage(topic, payload) {
    if (String(topic) === this.data.driverCode + "_OutTopic"){
      var msg = String(payload);
      var json = JSON.parse(msg);
      console.log(json)
      if (msg==="1") {
        this.setData({ switchChecked: false });
      } else {
        this.setData({ switchChecked: true })
      }
    }
  },
  onLoad: function () {
    this.setData({ "driverCode": app.globalData.driverCode});
    app.setConnectStatus(app.globalData.connectStatus);
    var outTopic = app.globalData.driverCode + "_OutTopic"
    app.subTopic(outTopic);
  }
})